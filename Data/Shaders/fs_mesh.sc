$input v_pos, v_view, v_normal, v_color0

/*
 * Copyright 2011-2014 Branimir Karadzic. All rights reserved.
 * License: http://www.opensource.org/licenses/BSD-2-Clause
 */

#include "include/common.sh"

uniform float3 fColor3;
uniform float3 fDirection3;
uniform float fAmbientIntensity1;

void main()
{    
    //calculate the vector from this pixels surface to the light source
    vec3 surfaceToLight = fDirection3 - v_pos;

    //calculate the cosine of the angle of incidence
    float brightness = dot(v_normal, surfaceToLight) / (length(surfaceToLight) * length(v_normal));
    brightness = clamp(brightness, 0, 1);

    gl_FragColor = vec4(brightness * fAmbientIntensity1 * fColor3, 1.0f);
}
