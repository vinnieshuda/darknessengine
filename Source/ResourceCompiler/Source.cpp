#include <iostream>
#include <string>
#include <stdio.h>
#include <osimplement.h>
#include <platform.h>
#include <tinydir.h>
#include <tinyxml2.h>
#include <map>
#include <list>

#ifdef ENGINE_PLATFORM_WINDOWS
#define POPEN _popen
#define PCLOSE _pclose
#else
#define POPEN popen
#define PCLOSE pclose
#endif // ENGINE_PLATFORM_WINDOWS

enum EResourceExt{
	Shader_Vertex = 0,
	Shader_Pixel,
	Image_PNG,
	Image_JPG,
	Mesh_Obj
};

enum EResourceType
{
	eRes_shader = 0,
	eRes_texture,
	eRes_mesh
};

typedef std::map<std::string, EResourceExt> ResourceMap;
typedef std::list<std::string> ResourceList;

bool replace(std::string& str, const std::string& from, const std::string& to) {
	size_t start_pos = str.find(from);
	if (start_pos == std::string::npos)
		return false;
	str.replace(start_pos, from.length(), to);
	return true;
}

bool exec(std::string cmd) 
{
	FILE* pipe = POPEN(cmd.c_str(), "r");
	if (!pipe) 
		return false;

	char buffer[128];
	while (!feof(pipe)) {
		if (fgets(buffer, 128, pipe) != NULL)
			std::cout << buffer << std::endl;
	}
	PCLOSE(pipe);
	return true;
}

void getResourceList(std::string path, ResourceMap& resMap)
{
	tinydir_dir dir;
	tinydir_open(&dir, path.c_str());

	while (dir.has_next)
	{
		tinydir_file file;
		tinydir_readfile(&dir, &file);

		// TODO. Check resources in subdirectory
		if (!file.is_dir)
		{
			std::string varying = "varying.def.sc";
			std::string tempStr = file.name;
			if (varying != tempStr)
			{	
				EResourceExt resExt;
				if (tempStr.find("vs_") != std::string::npos){
					resExt = Shader_Vertex;
					replace(tempStr, ".sc", "");
				}
				else if (tempStr.find("fs_") != std::string::npos){
					resExt = Shader_Pixel;
					replace(tempStr, ".sc", "");
				}

				resMap.insert(std::pair<std::string, EResourceExt>(tempStr, resExt));
			}			
		}

		tinydir_next(&dir);
	}

	tinydir_close(&dir);
}

int main()
{
	bool compComplete = false;
	tinyxml2::XMLDocument doc(true, tinyxml2::COLLAPSE_WHITESPACE);
	doc.LoadFile("rcConfig.xml");
	/*ResourceMap shadersList;
	ResourceList textureList;
	ResourceList meshList;*/
	ResourceMap resourceMap;

	printf("/***************  Resource Compiler Started  ***************/\n");

	/************************************************************************/
	/* RESOURCES                                                            */
	/************************************************************************/
	printf("/***************  Searching Shaders  ***************/\n");
	getResourceList("../../Data/Shaders", resourceMap);
	printf("DONE!!!\n");
	printf("/***************************************************/\n");
	//getResourceList("../../Data/Textures", textureList);
	//getResourceList("../../Data/Meshes", meshList);

	/************************************************************************/
	/* SHADER COMPILER		                                                */
	/************************************************************************/
	std::string includePath = " -i ";

	for (tinyxml2::XMLNode* ele = doc.FirstChildElement("shaders")->FirstChildElement("includedirs")->FirstChild(); ele; ele = ele->NextSibling())
	{
		includePath += ele->FirstChild()->Value();
		if (ele->NextSibling() != NULL)
			includePath += ";";
	}

	printf("/***************  Compiling Resources  ***************/\n");
	for (auto iter = resourceMap.begin(); iter != resourceMap.end(); iter++)
	{
		printf("/****  Compiling Shaders  ****/\n");
		if (iter->second == Shader_Vertex || iter->second == Shader_Pixel)
		{
			char* shaderType = "";
			char* shaderProfile = "";
			(iter->second == Shader_Vertex) ? shaderType = "v" : shaderType = "f";
			(iter->second == Shader_Vertex) ? shaderProfile = "vs_4_0" : shaderProfile = "ps_4_0";
			
			exec("shadercDebug" + includePath + " -f " + "../../Data/Shaders/" + iter->first + ".sc"+" -o " + "../../Data/" + iter->first + ".bin" + " --platform " + ENGINE_PLATFORM_STRING + " --type " + shaderType + " --profile " + shaderProfile);
		}
		printf("/****************************/\n");
	}
	/*for (auto iter = shadersList.begin(); iter != shadersList.end(); iter++)
	{	
		//std::cout << "Key: " << iter->c_str() << std::endl;
		exec("shadercDebug" + includePath + " -f " + "../../Data/Shaders/" + iter->c_str() + " -o " + "../../Data/" + iter->c_str() + ".bin" + " --platform " + ENGINE_PLATFORM_STRING + " --type " + "v" + " --profile " + "vs_3_0");
	}*/
	printf("/**************************************************/\n");
	printf("All resources compiles\n");
	while (compComplete==false)
	{	
	}

	return 0;
}