/************************************************************************
Filename: IEvetListener.h
Author: Andr�s Koncz
Created: 31/3/2014
************************************************************************/
#ifndef __IEVENTLISTENER_H__
#define __IEVENTLISTENER_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// forward
class IEventListener;

enum EEventType
{
	// Input Events
	eEvent_Input_KeyDown,
	eEvent_Input_KeyUp,

	eEvent_WindowResize
};

class IEventDispatcher
{
public:
	virtual bool AddListener(IEventListener* pListener) = 0;
	virtual bool RemoveListener(IEventListener* pListener) = 0;

	virtual void OnEvent(EEventType eventType, UINT_PTR wparam, UINT_PTR lparam) = 0;
};

class IEventListener
{
public:
	virtual void OnEvent(EEventType eventType, UINT_PTR wparam, UINT_PTR lparam) = 0;
};

#endif // __IEVENTLISTENER_H__