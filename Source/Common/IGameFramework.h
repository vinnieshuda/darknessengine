/************************************************************************
Filename: IGameFramework.h
Author: Andr�s Koncz
Created: 10/3/2014
************************************************************************/
#ifndef __IGAMEFRAMEWORK_H__
#define __IGAMEFRAMEWORK_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// Forwards
class IEngine;

class IGameFramework
{
public:
	virtual bool Init() = 0;
	virtual bool Run() = 0;
	virtual void Release() = 0;
};

#endif // !__IGAMEFRAMEWORK_H__
