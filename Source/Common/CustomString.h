#ifndef __CUSTOM_STRING_H__
#define __CUSTOM_STRING_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// TODO: Custom string
#define ENGINE_USE_STANDARD_STRING

#if defined(ENGINE_USE_STANDARD_STRING)
#include <string.h>
#include <string>
typedef std::string String;
#else
#error IMPLEMENT
#endif // defined(ENGINE_USE_STANDARD_STRING)

#endif // !__STRING_H__
