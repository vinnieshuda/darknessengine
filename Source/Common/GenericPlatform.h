/************************************************************************
Filename: GenericPlatform.h
Author: Andr�s Koncz
Created: 8/5/2014
************************************************************************/
#ifndef __GENERICPLATFORM_H__
#define __GENERICPLATFORM_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

struct GenericPlatformTypes
{
	typedef unsigned char			uint8;
	typedef unsigned short			uint16;
	typedef unsigned int			uint32;
	typedef unsigned long long		uint64;

	typedef signed char				int8;
	typedef signed short			int16;
	typedef signed int				int32;
	typedef signed long long		int64;
};

#endif // !__GENERICPLATFORM_H__
