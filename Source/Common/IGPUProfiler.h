/************************************************************************
Filename: IGPUProfiler.h
Author: Andr�s Koncz
Created: 18/9/2014
************************************************************************/
#ifndef __IGPUPROFILER_H__
#define __IGPUPROFILER_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

struct GPUInfos;

class IGPUProfiler
{
public:
	virtual GPUInfos* GetGPUInfo() = 0;
	virtual void ProfileStart(String name) = 0;
	virtual void ProfileEnd(String name) = 0;
};

#endif // !__IGPUPROFILER_H__
