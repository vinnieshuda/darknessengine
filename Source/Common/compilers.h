/************************************************************************
Filename: compilers.h
Author: Andr�s Koncz
Created: 8/5/2014
************************************************************************/
#ifndef __COMPILERS_H__
#define __COMPILERS_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// Functions
#define COMPILER_SUPPORT_CPP11 0
#define COMPILER_SUPPORT_STATIC_ASSERT 0
#define COMPILER_SUPPORT_PRAGMA_PACK_POP 0
#define COMPILER_SUPPORT_PRAGMA_ONCE 0

// Supported Compilers
#if defined(_MSC_VER) && _MSC_VER > 0
	#define ENGINE_COMPILER_VISUAL		_MSC_VER
#elif defined(__INTEL_COMPILER)
	#define ENGINE_COMPILER_INTEL		__INTEL_COMPILER
#elif defined(__GNUC__)
	#define ENGINE_COMPILER_GNUC
#else
	#error Unknown Compiler
#endif // defined(_MSC_VER)

// MSVC
#ifdef ENGINE_COMPILER_VISUAL
	#if ENGINE_COMPILER_VISUAL >= 1800
		#undef COMPILER_SUPPORT_CPP11
		#define COMPILER_SUPPORT_CPP11 1

		#undef COMPILER_SUPPORT_STATIC_ASSERT
		#define COMPILER_SUPPORT_STATIC_ASSERT 1

		#undef COMPILER_SUPPORT_PRAGMA_PACK_POP
		#define COMPILER_SUPPORT_PRAGMA_PACK_POP 1

		#undef COMPILER_SUPPORT_PRAGMA_ONCE
		#define COMPILER_SUPPORT_PRAGMA_ONCE 1
	#endif // ENGINE_COMPILER_VISUAL >= 1800
#endif // ENGINE_COMPILER_VISUAL


#endif // !__COMPILERS_H__
