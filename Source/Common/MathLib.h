/************************************************************************
Filename: MathLib.h
Author: Andr�s Koncz
Created: 9/3/2014
************************************************************************/
#ifndef __MATHLIB_H__
#define __MATHLIB_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#if defined(__USE_SSE__) && defined(ENGINE_PLATFORM_WINDOWS)
	#include "vectormathlibrary/sse/vectormath_aos.h"
#else
	#if defined(__USE_NEON__)
		#include "vectormathlibrary/neon/vectormath_aos.h"
	#else
		#include "vectormathlibrary/scalar/vectormath_aos.h"
	#endif
#endif // __USE_SSE__


// Math typedef
typedef Vectormath::Aos::Vector3	Vector3;
typedef Vectormath::Aos::Vector4	Vector4;
typedef Vectormath::Aos::Quat		Quat;
typedef Vectormath::Aos::Matrix3	Matrix3;
typedef Vectormath::Aos::Matrix4	Matrix4;
typedef Vectormath::Aos::Transform3	Transform3;
typedef Vectormath::Aos::Point3		Point3;

#endif // !__MATHLIB_H__
