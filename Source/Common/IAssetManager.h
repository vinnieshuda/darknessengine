#ifndef _IASSETMANAGER_H__
#define _IASSETMANAGER_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

class Entity;

class IAssetManager
{
	public:
    
    virtual bool Init() = 0;
	virtual void Release() = 0;

	virtual bool AddEntity(const char* _filePath) = 0;
	virtual void RemoveEntity(Entity* entity) = 0;

};

#endif