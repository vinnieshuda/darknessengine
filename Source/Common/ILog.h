/************************************************************************
Filename: IShader.h
Author: Andr�s Koncz
Created: 1/3/2014
************************************************************************/
#ifndef __ILOG_H__
#define __ILOG_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

class ILog
{
public:
	virtual int Init(const char* name) = 0;
	virtual void Print(const char* format, ...) = 0;
	virtual void PrintWarning(const char* format, ...) = 0;
	virtual void PrintError(const char* format, ...) = 0;	
};

#endif // __ILOG_H__