/************************************************************************
Filename: Color.h
Author: Andr�s Koncz
Created: 9/5/2014
************************************************************************/
#ifndef __COLOR_H__
#define __COLOR_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

class Color 
{
public:
	Color() { m_red = .0f; m_green = .0f; m_blue = .0f; m_alpha = 1.0f; }
	Color(float red, float green, float blue, float alpha = 1.0f) { m_red = red; m_green = green; m_blue = blue; m_alpha = alpha; }

	// For integers
	Color(int red, int green, int blue, int alpha = 255) 
	{ 
		// Convert integers value to float
		m_red = red / 255.0f;
		m_green = green / 255.0f;
		m_blue = blue / 255.0f;
		m_alpha = alpha / 255.0f;
	}

	// Float Specific
	void SetColors(float red, float green, float blue, float alpha = 1.0f) { m_red = red; m_green = green; m_blue = blue; m_alpha = alpha; }

	void SetRed(float red) { m_red = red; }
	void SetGreen(float green) { m_green = green; }
	void SetBlue(float blue) { m_blue = blue; }
	void SetAplha(float alpha) { m_alpha = alpha; }

	float GetRed() { return m_red; }
	float GetGreen() { return m_green; }
	float GetBlue() { return m_blue; }
	float GetAplha() { return m_alpha; }

	float* GetRGBA() 
	{ 
		float RGBA[4];
		RGBA[0] = m_red;
		RGBA[1] = m_green;
		RGBA[2] = m_blue;
		RGBA[3] = m_alpha;
		return &*RGBA; 
	}

	// Integer Specific
	void SetColors_Int(int red, int green, int blue, int alpha = 255) { m_red = red / 255.0f; m_green = green / 255.0f; m_blue = blue / 255.0f; m_alpha = alpha / 255.0f; }

	void SetRed_Int(int red) { m_red = red / 255.0f; }
	void SetGreen_Int(int green) { m_green = green / 255.0f; }
	void SetBlue_Int(int blue) { m_blue = blue / 255.0f; }
	void SetAplha_Int(int alpha) { m_alpha = alpha / 255.0f; }

	int GetRed_Int() { return (int)(m_red * 255.0); }
	int GetGreen_Int() { return (int)(m_green * 255.0); }
	int GetBlue_Int() { return (int)(m_blue * 255.0); }
	int GetAplha_Int() { return (int)(m_alpha * 255.0); }

	int* GetRGBA_Int()
	{
		int RGBA[4];
		RGBA[0] = GetRed_Int();
		RGBA[1] = GetGreen_Int();
		RGBA[2] = GetBlue_Int();
		RGBA[3] = GetAplha_Int();
		return &*RGBA;
	}

private:
	float m_red;
	float m_green;
	float m_blue;
	float m_alpha;
};

#include <BuiltinColors.h>

#endif // !__COLOR_H__
