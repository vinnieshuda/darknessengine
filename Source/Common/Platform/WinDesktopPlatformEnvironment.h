/************************************************************************
Filename: WinDesktopPlatformEnvironment.h
Author: Andr�s Koncz
Created: 8/5/2014
************************************************************************/
#ifndef __WIN32_DESKTOP_PLATFORM_ENVIRONMENT_H
#define __WIN32_DESKTOP_PLATFORM_ENVIRONMENT_H

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#include <direct.h>

namespace Platform
{
	class PlatformEnvironment
	{
	public:
		static String GetCurrentDir()
		{
			const size_t pathSize = 4096;
			char currentDirectory[pathSize];
			if (_getcwd(currentDirectory, pathSize) != NULL)
				return String(currentDirectory);
			else
				return String("");
		}

		static String GetEngineDir()
		{
			String currDir = GetCurrentDir();
			if (currDir.find("Release") != std::string::npos)
			{
				currDir.erase(currDir.find("\\Release"));
			}
			else if (currDir.find("Debug") != std::string::npos)
			{
				currDir.erase(currDir.find("\\Debug"));
			}

			if (currDir.find("Bin") != std::string::npos)
			{
				currDir.erase(currDir.find("Bin"));
			}
			return currDir;
		}
	};
}

#define FORCEINLINE __forceinline
#endif // !__WIN32_DESKTOP_PLATFORM_H
