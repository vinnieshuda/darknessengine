/************************************************************************
Filename: WinDesktopPlatformTypes.h
Author: Andr�s Koncz
Created: 8/5/2014
************************************************************************/
#ifndef __WIN_DESKTOP_PLATFORM_TYPES_H
#define __WIN_DESKTOP_PLATFORM_TYPES_H

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

namespace Platform
{
	struct PlatformTypes : public GenericPlatformTypes
	{
		typedef unsigned __int64		uint64;
		typedef signed __int64			int64;
	};
}

#define FORCEINLINE __forceinline
#endif // !__WIN32_DESKTOP_PLATFORM_H
