/************************************************************************
Filename: WinDesktopPlatformTimer.h
Author: Andr�s Koncz
Created: 19/9/2014
************************************************************************/
#ifndef __WIN32_DESKTOP_PLATFORM_TIMER_H
#define __WIN32_DESKTOP_PLATFORM_TIMER_H

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

namespace Platform
{
	class PlatformTimer
	{
	public:
		PlatformTimer()	
		{
			HANDLE threadHandle = GetCurrentThread();
			if (SetThreadAffinityMask(threadHandle, 1) == 0)
			{
				_ASSERT(false);
			}

			QueryPerformanceFrequency(&m_Frequency);

			Reset();
		}
		~PlatformTimer(){}

		void Reset()
		{
			LARGE_INTEGER largeInt;
			QueryPerformanceCounter(&largeInt);
			m_StartTime = largeInt.QuadPart;
		}

		double getMilliseconds()
		{
			QueryPerformanceCounter(&m_EndTime);

			return double(m_EndTime.QuadPart - m_StartTime) / (double(m_Frequency.QuadPart) / 1000.0);
		}

		double getMicroseconds()
		{
			QueryPerformanceCounter(&m_EndTime);

			return double(m_EndTime.QuadPart - m_StartTime) / (double(m_Frequency.QuadPart) / 1000000.0);
		}

		double getSeconds()
		{
			QueryPerformanceCounter(&m_EndTime);

			return double(m_EndTime.QuadPart - m_StartTime) / (double(m_Frequency.QuadPart));
		}

	private:
		LARGE_INTEGER m_Frequency;
		Platform::PlatformTypes::int64 m_StartTime;
		LARGE_INTEGER m_EndTime;
	};
}
#endif // !__WIN32_DESKTOP_PLATFORM_TIMER_H
