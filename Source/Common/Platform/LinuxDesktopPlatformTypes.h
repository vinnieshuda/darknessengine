/************************************************************************
Filename: LinuxDesktopPlatformTypes.h
Author: Andr�s Koncz
Created: 8/5/2014
************************************************************************/
#ifndef __LINUX_DESKTOP_PLATFORM_TYPES_H__
#define __LINUX_DESKTOP_PLATFORM_TYPES_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

namespace Platform
{
	struct PlatformTypes : public GenericPlatformTypes
	{
	};
}

typedef void* HWND;
typedef void* HINSTANCE;

#define FORCEINLINE inline __attribute__ ((always_inline))

#endif // !__LINUX_DESKTOP_PLATFORM_H__
