#ifndef _IMESH_H__
#define _IMESH_H__
#endif

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

class Mesh;

class IMesh
{
public:

	virtual void Load(const char* _filePath) = 0;
	virtual void UnLoad() = 0;
	virtual void Draw(uint8_t _id, bgfx::ProgramHandle _program,/* float* _mtx,*/ uint64_t _state) = 0;

};
