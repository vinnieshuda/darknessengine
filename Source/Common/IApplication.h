/************************************************************************
Filename: platform.h
Author: Andr�s Koncz
Created: 6/3/2014
************************************************************************/
#ifndef __IAPPLICATION_H__
#define __IAPPLICATION_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

//Forwards
struct InitParams;
class IRenderSystem;

class IApplication
{
public:
	virtual bool Init(InitParams& initParams) = 0;
	virtual bool Run() = 0;
	virtual void Release() = 0;
	virtual HWND GetHWND() = 0;
	virtual void RegisterRenderSystem(IRenderSystem* pRenderSystem) = 0;

	virtual int GetWindowWidth() = 0;
	virtual int GetWindowHeight() = 0;
	virtual bool IsFullscreen() = 0;
};

#endif // !__IAPPLICATION_H__
