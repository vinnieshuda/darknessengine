/************************************************************************
Filename: platform.h
Author: Andr�s Koncz
Created: 5/3/2014
************************************************************************/
#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#include <osimplement.h>
#include <compilers.h>
#include <GenericPlatform.h>
#include <StaticAssert.h>
#include <EngineGlobals.h>

// STL
#include <CustomString.h>

#if defined(ENGINE_PLATFORM_WINDOWS) || defined(ENGINE_PLATFORM_LINUX)
	#define ENGINE_PLATFORM_DESKTOP
#endif

// Platform Specific Includes
#if defined(ENGINE_PLATFORM_WINDOWS)
	#include <Platform/WinDesktopPlatformTypes.h>
	#include <Platform/WinDesktopPlatformEnvironment.h>
	#include <Platform/WinDesktopPlatformTimer.h>
#endif // defined(ENGINE_PLATFORM_WINDOWS)

#if defined(ENGINE_PLATFORM_LINUX)
	#include <Platform/LinuxDesktopPlatformTypes.h>
#endif // defined(ENGINE_PLATFORM_LINUX)

/************************************************************************/
/*					Platform Specific Types                             */
/************************************************************************/
typedef Platform::PlatformTypes::uint8		uint8;
typedef Platform::PlatformTypes::uint16		uint16;
typedef Platform::PlatformTypes::uint32		uint32;
typedef Platform::PlatformTypes::uint64		uint64;

typedef Platform::PlatformTypes::int8		int8;
typedef Platform::PlatformTypes::int16		int16;
typedef Platform::PlatformTypes::int32		int32;
typedef Platform::PlatformTypes::int64		int64;

CompileTimeAssert(sizeof(uint8) == 1,	"Incorrect BYTE size");
CompileTimeAssert(sizeof(uint16) == 2,	"Incorrect WORD size");
CompileTimeAssert(sizeof(uint32) == 4,	"Incorrect DWORD size");
CompileTimeAssert(sizeof(uint64) == 8,	"Incorrect QWORD size");

CompileTimeAssert(sizeof(int8) == 1,	"Incorrect signed BYTE size");
CompileTimeAssert(sizeof(int16) == 2,	"Incorrect signed WORD size");
CompileTimeAssert(sizeof(int32) == 4,	"Incorrect signed DWORD size");
CompileTimeAssert(sizeof(int64) == 8,	"Incorrect signed QWORD size");

typedef Platform::PlatformEnvironment		Environment;
typedef Platform::PlatformTimer				HiResTimer;

#endif // !__PLATFORM_H__
