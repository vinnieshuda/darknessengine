#ifndef __STATIC_ASSERT_H__
#define __STATIC_ASSERT_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// TODO: Compile Time assert! Maybe Boost Static Assert?

#if COMPILER_SUPPORT_STATIC_ASSERT
	#define CompileTimeAssert(expr, msg) static_assert(expr, #msg)
#else
#error IMPLEMENT;
#endif // ENGINE_SUPPORT_STATIC_ASSERT


#endif // !__STATIC_ASSERT_H__
