/************************************************************************
Filename: IEngine.h
Author: András Koncz
Created: 27/2/2014
************************************************************************/
#ifndef __IENGINE_H__
#define __IENGINE_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)
#include <platform.h>
#include <IRenderSystem.h>
#include <IGPUProfiler.h>
#include <IAssetManager.h>

#ifdef ENGINE_EXPORT
#define ENGINE_API DLL_EXPORT
#else
#define ENGINE_API DLL_IMPORT
#endif

// Engine Version
#define ENGINE_VERSION_MAJOR 0
#define ENGINE_VERSION_MINOR 1
#define ENGINE_VERSION_PATCH 0

// Forward Declarations
class ILog;
class IApplication;
class IEventDispatcher;
class IAssetManager;

enum ECPUVendor
{
	eCPU_AMD,
	eCPU_Intel,
	eCPU_ARM,

	eCPU_UNKNOWN
};

struct InitParams
{
	HINSTANCE mHinstance;
	const char* Title;
};

class IEngineConfig
{
public:
	virtual int GetInt(const char* key, int defaultValue = 0) = 0;
	virtual bool GetBool(const char* key, bool defaultValue = false) = 0;
	virtual float GetFloat(const char* key, float defaultValue = 0.0f) = 0;
	virtual const char* GetCString(const char* key, const char* defaultValue = "UNKNOWN") = 0;
	virtual std::string GetString(const char* key, std::string defaultValue = "UNKNOWN") = 0;

	virtual bool SetInt(const char* key, int value) = 0;
	virtual bool SetBool(const char* key, bool value) = 0;
	virtual bool SetFloat(const char* key, float value) = 0;
	virtual bool SetCString(const char* key, const char* value) = 0;
	virtual bool SetString(const char* key, std::string value) = 0;

	virtual bool keyExists(const std::string &key) const = 0;

	virtual bool Save() = 0;
};

class IEngine
{
public:
	virtual bool Init(InitParams& initParams) = 0;
	virtual bool Run() = 0;
	virtual void Release() = 0;

	virtual IRenderSystem* getRenderSystem() = 0;
	virtual IAssetManager* getIAssetManager() = 0;
	virtual ILog* GetILog() = 0;
	virtual IEngineConfig* GetCFG() = 0;
	virtual IApplication* GetIApp() = 0;
	virtual IEventDispatcher* GetEventDispatcher() = 0;
	virtual IGPUProfiler* GetIGPUProfiler() = 0;
};

//////////////////////////////////////////////////////////////////////////
// ENGINE GLOBALS
//////////////////////////////////////////////////////////////////////////
//extern EngineGlobals* pGlobals;

//////////////////////////////////////////////////////////////////////////
// Entry point
//////////////////////////////////////////////////////////////////////////
typedef IEngine* (*PFNCREATEENGINEINTERFACE)(InitParams& initParams);

extern "C"
{
	ENGINE_API IEngine* CreateEngineInterface(InitParams& initParams);
}

#endif // __IENGINE_H__