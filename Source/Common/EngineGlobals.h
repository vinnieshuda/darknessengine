/************************************************************************
Filename: EngineGlobals.h
Author: Andr�s Koncz
Created: 5/3/2014
************************************************************************/
#ifndef __ENGINEGLOBALS_H__
#define __ENGINEGLOBALS_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// forwards
class IEngine;
class ILog;
class IRenderSystem;

struct EngineGlobals
{
	IEngine* pEngine;
	ILog* pLog;
	IRenderSystem* pRenderer;
};

#endif // !__ENGINEGLOBALS_H__
