#ifndef _IENTITY_H__
#define _IENTITY_H__
#endif

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

class IEntity
{
public:
	IEntity(){}
	virtual ~IEntity(){}

	virtual bool Update(float time) = 0;
	virtual void Draw() = 0;
	virtual void Destroy() = 0;

};