/************************************************************************
Filename: osimplement.h
Author: Andr�s Koncz
Created: 5/3/2014
************************************************************************/
#ifndef __OSIMPLEMENT_H__
#define __OSIMPLEMENT_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#if defined(_DEBUG) || defined(DEBUG)
	#ifndef NDEBUG
		#define ENGINE_DEBUG
	#endif // !NDEBUG
#endif

#if defined(_LIB)
	#define ENGINE_STATIC_LIB
#endif

#if defined(_TOOL_BUILD)
#define ENGINE_TOOL_BUILD
#endif

#if defined(_WIN32) || defined(_WIN64)
	#define ENGINE_PLATFORM_WINDOWS
	#define ENGINE_PLATFORM_STRING "windows"
	#define NOMINMAX
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#define API_FUNC GetProcAddress
	#define INLINE __forceinline
#elif defined(WINRT)
	#define ENGINE_PLATFORM_WINRT
	#define ENGINE_PLATFORM_STRING "windows"
	#ifdef PHONE
		#define ENGINE_WINRT_TARGET_PHONE
	#endif // PHONE

#elif defined(_LINUX)
	#define ENGINE_PLATFORM_LINUX
	#define ENGINE_PLATFORM_STRING "linux"
	#include <dlfcn.h>
	#define API_FUNC dlsym
	#define INLINE __inline__

#else
	#define ENGINE_PLATFORM_UNKNOWN
	#define ENGINE_PLATFORM_STRING "UNKNOWN"
	#define API_FUNC
#endif

#if defined(ENGINE_PLATFORM_LINUX) || defined(ENGINE_PLATFORM_PS3)
	#define DLL_EXPORT __attribute__ ((visibility("default")))
	#define DLL_IMPORT __attribute__ ((visibility("default")))
#elif defined(ENGINE_PLATFORM_XBOX360)
	#define DLL_EXPORT 
	#define DLL_IMPORT 
#elif defined(ENGINE_PLATFORM_WINDOWS)
	#ifdef ENGINE_STATIC_LIB
		#define DLL_EXPORT
		#define DLL_IMPORT
	#else
		#define DLL_EXPORT __declspec(dllexport)
		#define DLL_IMPORT __declspec(dllimport)
	#endif // ENGINE_STATIC_LIB
#else
	#define DLL_EXPORT
	#define DLL_IMPORT
#endif

#if defined(__ia64__) || defined(_M_IA64) || \
	defined(__ppc64__) || defined(__PPC64__) || \
	defined(__arch64__) || \
	defined(__x86_64__) || defined(_M_X64) || defined(_M_AMD64) || \
	defined(__LP64__) || defined(__LLP64) || \
	defined(_WIN64) || \
	(__WORDSIZE == 64)
#define ENGINE_CPU_64BIT
#define ENGINE_CPU_BITSTRING "64"
#else
#define ENGINE_CPU_32BIT
#define ENGINE_CPU_BITSTRING "32"
#endif

#endif // !__OSIMPLEMENT_H__