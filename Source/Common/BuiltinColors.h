/************************************************************************
Filename: Color.h
Author: Andr�s Koncz
Created: 10/5/2014
************************************************************************/
#ifndef __BUILTINCOLORS_H__
#define __BUILTINCOLORS_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

namespace BaseColors
{
	INLINE Color Black() { return Color(0, 0, 0); }

	INLINE Color Red() { return Color(255, 0, 0); }
	INLINE Color IndianRed() { return Color(176, 23, 31); }
	INLINE Color PalevioletRed() { return Color(219, 112, 147); }
	INLINE Color VioletRed() { return Color(208, 32, 144); }

	INLINE Color Pink() { return Color(255, 192, 203); }
	INLINE Color LightPink() { return Color(255, 182, 193); }
	INLINE Color HotPink() { return Color(255, 105, 180); }
	INLINE Color DeepPink() { return Color(255, 20, 147); }

	INLINE Color Magenta() { return Color(255, 0, 255); }

	INLINE Color Purple() { return Color(128, 0, 128); }
	INLINE Color MediumPurple() { return Color(147, 112, 219); }

	INLINE Color Indigo() { return Color(75, 0, 130); }

	INLINE Color Blue() { return Color(0, 0, 255); }
	INLINE Color SlateBlue() { return Color(106, 90, 205); }
	INLINE Color MediumBlue() { return Color(0, 0, 205); }
	INLINE Color DarkBlue() { return Color(0, 0, 139); }
	INLINE Color NavyBlue() { return Color(0, 0, 128); }
	INLINE Color RoyalBlue() { return Color(65, 105, 225); }
	INLINE Color Cobalt() { return Color(61, 89, 171); }
	INLINE Color DeepSkyBlue() { return Color(0, 191, 255); }
	INLINE Color Aqua() { return Color(0, 255, 255); }
	INLINE Color Aquamarine() { return Color(127, 255, 212); }

	INLINE Color White() { return Color(255, 255, 255); }
	INLINE Color GhostWhite() { return Color(248, 248, 255); }
	INLINE Color WhiteSmoke() { return Color(245, 245, 245); }

	INLINE Color Green() { return Color(0, 255, 0); }
	INLINE Color DarkGreen() { return Color(0, 100, 0); }
	INLINE Color SapGreen() { return Color(48, 128, 20); }
	INLINE Color GreenYellow() { return Color(173, 255, 47); }

	INLINE Color Yellow() { return Color(255, 255, 0); }
	INLINE Color Gold() { return Color(255, 215, 0); }
	INLINE Color CadmiumYellow() { return Color(255, 153, 18); }

	INLINE Color Orange() { return Color(255, 128, 0); }

	INLINE Color Sepia() { return Color(94, 38, 18); }
}

#endif // !__BUILTINCOLORS_H__
