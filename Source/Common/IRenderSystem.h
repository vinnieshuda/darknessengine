/************************************************************************
Filename: IRenderSystem.h
Author: Andr�s Koncz
Created: 27/2/2014
************************************************************************/
#ifndef __IRENDERSYSTEM_H__
#define __IRENDESYSTEM_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)


struct GPUInfos
{
	GPUInfos() {}
	String GPUDesc;
	UINT VendorID;
	UINT DeviceID;
};

enum EGPUVendor
{
	eGPUVendor_AMD = 0x1002,
	eGPUVendor_Nvidia = 0x10DE,
	eGPUVendor_Intel = 0x8086
};

enum EDriverType
{
	eDriver_NULL,
	eDriver_D3D9,
	eDriver_D3D10,
	eDriver_D3D11,
	eDriver_OpenGL
};

class IRenderProfiler
{
public:
	virtual void ProfileStart(String name) = 0;
	virtual void ProfileEnd(String name) = 0;
	virtual void EndFrame() = 0;
};

class IRenderSystem
{
public:
	virtual bool Init(int screenWidth, int screenHeight, HWND hwnd, bool fullscreen) = 0;
	virtual void Release() = 0;
	virtual void Frame() = 0;
};

#endif // __IRENDERSYSTEM_H__