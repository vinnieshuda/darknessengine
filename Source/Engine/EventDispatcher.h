#ifndef __CEVENTDISPATCHER_H__
#define __CEVENTDISPATCHER_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#include <list>

class CEventDispatcher : public IEventDispatcher
{
public:
	CEventDispatcher();
	virtual ~CEventDispatcher();

	// IEventDispatcher
	virtual bool AddListener(IEventListener* pListener);
	virtual bool RemoveListener(IEventListener* pListener);

	virtual void OnEvent(EEventType eventType, UINT_PTR wparam, UINT_PTR lparam);
	// ~IEventDispatcher

private:
	typedef std::list<IEventListener*> EventListeners;
	EventListeners m_eventListeners;
};

#endif // !__CEVENTDISPATCHER_H__