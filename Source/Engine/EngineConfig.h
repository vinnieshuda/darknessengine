#ifndef __CENGINECONFIG_H__
#define __CENGINECONFIG_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)
#include <map>

class CEngineConfig : public IEngineConfig
{
public:
	CEngineConfig(const char* name);
	virtual ~CEngineConfig();

	// IEngineConfig
	virtual int GetInt(const char* key, int defaultValue = 0);
	virtual bool GetBool(const char* key, bool defaultValue = false);
	virtual float GetFloat(const char* key, float defaultValue = 0.0f);
	virtual const char* GetCString(const char* key, const char* defaultValue = "UNKNOWN");
	virtual std::string GetString(const char* key, std::string defaultValue = "UNKNOWN");

	virtual bool SetInt(const char* key, int value);
	virtual bool SetBool(const char* key, bool value);
	virtual bool SetFloat(const char* key, float value);
	virtual bool SetCString(const char* key, const char* value);
	virtual bool SetString(const char* key, std::string value);

	virtual bool keyExists(const std::string &key) const;

	virtual bool Save();
	// ~IEngineConfig

	bool Open(const char* name);
	bool parseCFG();
	void parseLine(const std::string& line, unsigned int lineNumber);
	void getCFGValue(const std::string& line, unsigned int lineNumber);
	void extractValue(std::string &cfgValue, size_t const &sepPos, const std::string &line) const;
	void extractKey(std::string &key, size_t const &sepPos, const std::string &line) const;
	bool IsWhitespace(std::string& line);
	void removeComment(std::string& line);
	void setCFGValue(const char* key, std::string value);

	template <typename T>
	T string_to_T(std::string const &val);

	template <typename T>
	std::string T_to_string(T const &val);


private:
	std::map<std::string, std::string> contents;
	const char* fileName;
};

#endif // __CENGINECONFIG_H__