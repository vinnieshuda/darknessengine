#include "stdafx.h"
#include "EngineConfig.h"
#include <fstream>
#include <sstream>

CEngineConfig::CEngineConfig(const char* name)
{
	Open(name);
}

CEngineConfig::~CEngineConfig()
{
}

bool CEngineConfig::Open(const char* name)
{
	fileName = name;

	if (!parseCFG())
	{
		CEngine::Instance()->GetILog()->PrintError("[CFG] parsing failed");
		return false;
	}

	return true;
}

bool CEngineConfig::Save()
{
	std::ofstream fileStreamOut(fileName);
	std::string lines;
	if (!fileStreamOut.is_open())
	{
		CEngine::Instance()->GetILog()->PrintError("[CFG] (%s) not found", fileName);
		return false;
	}

	for (auto it = contents.begin(); it != contents.end(); it++)
	{
		std::ostringstream os;
		os << it->first << "=" << it->second.c_str() << "\n";
		lines += os.str();
	}

	fileStreamOut << lines;
	fileStreamOut.close();

	return true;
}

bool CEngineConfig::parseCFG()
{
	std::ifstream fileStream(fileName);
	if (!fileStream.is_open())
	{
		CEngine::Instance()->GetILog()->PrintError("[CFG] (%s) not found", fileName);
		return false;
	}

	std::string line;
	unsigned int lineNumber = 0;
	while (std::getline(fileStream, line))
	{
		std::string temp = line;
		lineNumber++;

		if (temp.empty())
			continue;

		removeComment(temp);
		if (IsWhitespace(temp))
			continue;

		parseLine(temp, lineNumber);
	}

	fileStream.close();
	
	return true;
}

void CEngineConfig::parseLine(const std::string& line, unsigned int lineNumber)
{
	if (line.find("=") == line.npos)
		CEngine::Instance()->GetILog()->PrintError("[CFG] Couldn't find separator on line: %s\n", lineNumber);
	else
		getCFGValue(line, lineNumber);
}

void CEngineConfig::getCFGValue(const std::string& line, unsigned int lineNumber)
{
	std::string temp = line;
	temp.erase(0, temp.find_first_not_of("\t "));
	size_t sepPos = temp.find('=');

	std::string key, value;
	extractKey(key, sepPos, temp);
	extractValue(value, sepPos, temp);

	if (!keyExists(key))
		contents.insert(std::pair<std::string, std::string>(key, value));
	else
		CEngine::Instance()->GetILog()->PrintError("[CFG] Multiple defined key in: %i!\n", lineNumber);
}

bool CEngineConfig::keyExists(const std::string &key) const
{
	return contents.find(key) != contents.end();
}

void CEngineConfig::removeComment(std::string& line)
{
	if (line.find("//") != line.npos)
	{
		line.erase(line.find("//"));
	}
}

bool CEngineConfig::IsWhitespace(std::string& line)
{
	return (line.find_first_not_of(" ") == line.npos);
}

void CEngineConfig::extractKey(std::string &key, size_t const &sepPos, const std::string &line) const
{
	key = line.substr(0, sepPos);
	if (key.find('\t') != line.npos || key.find(' ') != line.npos)
		key.erase(key.find_first_of("\t "));
}
void CEngineConfig::extractValue(std::string &cfgValue, size_t const &sepPos, const std::string &line) const
{
	cfgValue = line.substr(sepPos + 1);
	cfgValue.erase(0, cfgValue.find_first_not_of("\t "));
	cfgValue.erase(cfgValue.find_last_not_of("\t ") + 1);
}

void CEngineConfig::setCFGValue(const char* key, std::string value)
{
	if (keyExists(key))
	{
		std::string& temp = contents.find(key)->second;
		temp = value;
	}
	else
	{
		contents.insert(std::pair<std::string, std::string>(key, value));
	}
}

template <typename T>
T CEngineConfig::string_to_T(std::string const &val)
{
	std::istringstream istr(val);
	T returnVal;
	if (!(istr >> returnVal))
		CEngine::Instance()->GetILog()->PrintError("[CFG] Not a valid %s received!\n", (std::string)typeid(T).name());

	return returnVal;
}

template <typename T>
std::string CEngineConfig::T_to_string(T const &val)
{
	std::ostringstream ostr;
	ostr << val;

	return ostr.str();
}

//////////////////////////////////////////////////////////////////////////
// Get functions
//////////////////////////////////////////////////////////////////////////
int CEngineConfig::GetInt(const char* key, int defaultValue /* = 0 */)
{
	if (!keyExists(key))
	{
		CEngine::Instance()->GetILog()->PrintWarning("[CFG] The key(%s) is invalid", key);
		return defaultValue;
	}

	return string_to_T<int>(contents.find(key)->second);
}

bool CEngineConfig::GetBool(const char* key, bool defaultValue /* = false */)
{
	if (!keyExists(key))
	{
		CEngine::Instance()->GetILog()->PrintWarning("[CFG] The key(%s) is invalid", key);
		return defaultValue;
	}

	return string_to_T<bool>(contents.find(key)->second);
}

float CEngineConfig::GetFloat(const char* key, float defaultValue /* = 0.0f */)
{
	if (!keyExists(key))
	{
		CEngine::Instance()->GetILog()->PrintWarning("[CFG] The key(%s) is invalid", key);
		return defaultValue;
	}

	return string_to_T<float>(contents.find(key)->second);
}

const char* CEngineConfig::GetCString(const char* key, const char* defaultValue /* = "" */)
{
	if (!keyExists(key))
	{
		CEngine::Instance()->GetILog()->PrintWarning("[CFG] The key(%s) is invalid", key);
		return defaultValue;
	}

	const std::string& retVal = contents.find(key)->second;

	return retVal.c_str();
}

std::string CEngineConfig::GetString(const char* key, std::string defaultValue /* = "" */)
{
	if (!keyExists(key))
	{
		CEngine::Instance()->GetILog()->PrintWarning("[CFG] The key(%s) is invalid", key);
		return defaultValue;
	}

	return contents.find(key)->second;
}

//////////////////////////////////////////////////////////////////////////
// Set Functions
//////////////////////////////////////////////////////////////////////////
bool CEngineConfig::SetInt(const char* key, int value)
{
	setCFGValue(key, T_to_string<int>(value));

	return true;
}

bool CEngineConfig::SetBool(const char* key, bool value)
{
	setCFGValue(key, T_to_string<bool>(value));

	return true;
}

bool CEngineConfig::SetFloat(const char* key, float value)
{
	setCFGValue(key, T_to_string<float>(value));

	return true;
}

bool CEngineConfig::SetCString(const char* key, const char* value)
{
	setCFGValue(key, T_to_string<const char*>(value));

	return true;
}

bool CEngineConfig::SetString(const char* key, std::string value)
{
	setCFGValue(key, value);

	return true;
}