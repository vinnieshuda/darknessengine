#ifndef __CLOG_H__
#define __CLOG_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// Forward Declarations
class ILog;

class CLog : public ILog
{
public:
	CLog();
	CLog(const char* name);
	virtual ~CLog();
	
	// ILog
	virtual int Init(const char* name);
	virtual void Print(const char* format, ...);
	virtual void PrintWarning(const char* format, ...);
	virtual void PrintError(const char* format, ...);
	// ~ILog
};

#endif // __CLOG_H__