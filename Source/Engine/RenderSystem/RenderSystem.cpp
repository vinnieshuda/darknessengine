#include "stdafx.h"
#include "RenderSystem.h"

#include <bgfxplatform.h>

#include <Color.h>
#include <IGPUProfiler.h>

static HiResTimer timer;

RenderSystem::RenderSystem()
{
	timer.Reset();

}

RenderSystem::~RenderSystem()
{
}

bool RenderSystem::Init(int screenWidth, int screenHeight, HWND hwnd, bool fullscreen)
{
	bgfx::winSetHwnd(hwnd);
	uint32_t debug = BGFX_DEBUG_STATS;
	uint32_t reset = /*BGFX_RESET_VSYNC*/BGFX_RESET_NONE;
	bgfx::init();
	bgfx::reset(screenWidth, screenHeight, reset);

	// Enable debug text.
	bgfx::setDebug(debug);

	// Set view 0 clear state.
	bgfx::setViewClear(0
		, BGFX_CLEAR_COLOR_BIT | BGFX_CLEAR_DEPTH_BIT
		, 0x303030ff
		, 1.0f
		, 0
		);
	sWidth = screenWidth;
	sHeight = screenHeight;

	//Shader Init
	light = new LightEntity();
	light->CreateMesh(String(Environment::GetEngineDir() + "Data/cube.bin").c_str());
	light->getMesh()->scale(0.1f, 0.1f, 0.1f);
	light->setPosition(0.0f, 0.0f, 0.0f);
	light->setRotation(0.0f, 0.0f, 0.0f);
	light->setColor(0.0f, 1.3f, 0.0f);
	light->setDirection(0.0f, 0.0f, -4.0f);
	light->setIntensity(0.1f);
	light->setUniforms();
	rainbowMeshShader = CEngine::Instance()->getAssetManager()->loadShaders(String(Environment::GetEngineDir() + "Data/vs_mesh.bin").c_str(), String(Environment::GetEngineDir() + "Data/fs_mesh.bin").c_str());
	return true;
}

void RenderSystem::Update()
{

}

void RenderSystem::Render()
{


}

void RenderSystem::Frame()
{
	// Set view 0 default viewport.
	bgfx::setViewRect(0, 0, 0, sWidth, sHeight);

	// This dummy draw call is here to make sure that view 0 is cleared
	// if no other draw calls are submitted to view 0.
	bgfx::submit(0);

	// Use debug font to print information about this example.
	bgfx::dbgTextClear();
	bgfx::dbgTextPrintf(0, 1, 0x4f, "DarknessEngine test");
	bgfx::dbgTextPrintf(0, 2, 0x6f, "We love Titov!");

	float at[3] = { 0.0f, 0.0f, 0.0f };
	float eye[3] = { 0.0f, 0.0f, -5.5f };

	// Set view and projection matrix for view 0.
	const bgfx::HMD* hmd = bgfx::getHMD();
	if (NULL != hmd)
	{
		float view[16];
		bx::mtxQuatTranslationHMD(view, hmd->eye[0].rotation, eye);

		float proj[16];
		bx::mtxProj(proj, hmd->eye[0].fov, 0.1f, 10.0f);

		bgfx::setViewTransform(0, view, proj);

		// Set view 0 default viewport.
		//
		// Use HMD's width/height since HMD's internal frame buffer size
		// might be much larger than window size.
		bgfx::setViewRect(0, 0, 0, hmd->width, hmd->height);
	}
	else
	{
		float view[16];
		bx::mtxLookAt(view, eye, at);

		float proj[16];
		bx::mtxProj(proj, 60.0f, float(sWidth) / float(sHeight), 0.1f, 100.0f);
		bgfx::setViewTransform(0, view, proj);

		// Set view 0 default viewport.
		bgfx::setViewRect(0, 0, 0, sWidth, sHeight);

	}
	float time = timer.getSeconds();
	light->setDirection(/*-cos(time) * 8.0f*/light->direction[0], light->direction[1] /*-sin(time) * 8.0f*/, -cos(time) * 16.0f);
	light->setUniforms();
	for (auto it = CEngine::Instance()->getAssetManager()->getEntityList()->begin(); it != CEngine::Instance()->getAssetManager()->getEntityList()->end(); ++it)
	{

		it->getMesh()->Draw(0, rainbowMeshShader);
		//it->setRotation(it->randomseed * time);
	}
	light->getMesh()->Draw(0, rainbowMeshShader);
/*
	if (!CEngine::Instance()->getAssetManager()->MeshMap.empty())
		for (auto it = CEngine::Instance()->getAssetManager()->MeshMap.begin(); it != CEngine::Instance()->getAssetManager()->MeshMap.end(); ++it)
		{
			bgfx::dbgTextPrintf(0, 3, 0x4f, "ObjectPos:%f , %f, %f ObjectMap size: %d", it->second->getPos().x, it->second->getPos().y, it->second->getPos().z, CEngine::Instance()->getAssetManager()->MeshMap.size());
			
			float time = timer.getSeconds();
			it->second->rotateXY(it->second->getPos().x * time, it->second->getPos().y * time);
			it->second->Draw(0, rainbowMeshShader);
		}*/

	// Advance to next frame. Rendering thread will be kicked to 
	// process submitted rendering primitives.
	bgfx::frame();
}

void RenderSystem::Release()
{
	bgfx::destroyProgram(rainbowMeshShader);
	bgfx::shutdown();
}