#ifndef __RENDER_H__
#define __RENDER_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#include "IRenderSystem.h"
#include "Light.h"

class Mesh;

class RenderSystem : public IRenderSystem
{
public:
	RenderSystem();
	virtual ~RenderSystem();

	// IRenderSystem
	virtual bool Init(int screenWidth, int screenHeight, HWND hwnd, bool fullscreen);
	virtual void Update();
	virtual void Render();
	virtual void Release();

	virtual void Frame();

	private:
		int sWidth;
		int sHeight;

	// Shaders
		LightEntity* light;
		bgfx::ProgramHandle rainbowMeshShader;

};

#endif // __D3D11RENDER_H__