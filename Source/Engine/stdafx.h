#ifndef __stdafx_h__
#define __stdafx_h__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#define ENGINE_EXPORT

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <platform.h>

#if !defined(ENGINE_PLATFORM_PS3)
#include <memory.h>
#include <malloc.h>
#endif

#if !defined(ENGINE_PLATFORM_PS3)
#if defined( ENGINE_PLATFORM_LINUX )
#	include <sys/io.h>
#else
#	include <io.h>
#endif
#endif

#ifdef ENGINE_PLATFORM_WINDOWS
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <tlhelp32.h>
#undef GetCharWidth
#undef GetUserName
#endif

//////////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////////
#include <IEngine.h>
#include <IRenderSystem.h>
#include <ILog.h>
#include <IApplication.h>
#include <IEventListener.h>


//////////////////////////////////////////////////////////////////////////
// Mathlib
//////////////////////////////////////////////////////////////////////////
#include <MathLib.h>

//////////////////////////////////////////////////////////////////////////
// Engine Include
//////////////////////////////////////////////////////////////////////////
#include "Engine.h"
#include "SystemInfo.h"

//////////////////////////////////////////////////////////////////////////
// BGFX Include
//////////////////////////////////////////////////////////////////////////
#include "common.h"
#include <bgfx.h>
#include "AssetManager.h"
#endif //__stdafx_h__