#ifndef __CENGINE_H__
#define __CENGINE_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

// Forward Declarations
class IRenderSystem;
class IApplication;
class AssetManager;

class CEngine : public IEngine
{
public:
	static CEngine* Instance();

	CEngine();
	~CEngine();

	// IEngine
	virtual bool Init(InitParams& initParams);
	virtual bool Run();
	virtual void Release();

	virtual IRenderSystem* getRenderSystem();
	virtual IAssetManager* getIAssetManager(){ return m_pIAssetManager; }
	virtual ILog* GetILog();
	virtual IEngineConfig* GetCFG();
	virtual IApplication* GetIApp();
	virtual IEventDispatcher* GetEventDispatcher();
	virtual IGPUProfiler* GetIGPUProfiler() { return m_pGPUProfiler; }
	// ~IEngine	

	AssetManager* getAssetManager() { return m_pAssetManager; }

	bool InitializeEventDispatcher();

private:
	IRenderSystem* m_pRenderSystem;
	AssetManager* m_pAssetManager;
	IAssetManager* m_pIAssetManager;
	static CEngine* m_pEngine;
	ILog* m_pLog;
	IEngineConfig* m_pEngineCFG;
	IApplication* m_pApplication;
	IEventDispatcher* m_pEventDispatcher;
	IGPUProfiler* m_pGPUProfiler;

	HiResTimer m_Timer;
};

#endif