#include "stdafx.h"
#include "GPUProfiler.h"

CGPUProfiler::CGPUProfiler(IRenderSystem* pRenderSystem)
{
	m_pRenderSystem = pRenderSystem;
}

CGPUProfiler::~CGPUProfiler()
{
}

void CGPUProfiler::ProfileStart(String name)
{
	if (CEngine::Instance()->GetCFG()->GetBool("r_enableProfiler") && CEngine::Instance()->GetCFG()->GetBool("r_enableRenderProfiler"))
	{
		m_pRenderSystem->GetRenderProfiler()->ProfileStart(name);
	}
	else if (CEngine::Instance()->GetCFG()->GetBool("r_enableProfiler") && CEngine::Instance()->GetCFG()->GetBool("r_enableHardwareProfiler"))
	{
		// IssEmu::Todo - AMD GPUPerfapi | NVIDIA PerfKit
		throw std::logic_error("not implemented yet.");
	}
	
	return;
}

void CGPUProfiler::ProfileEnd(String name)
{
	if (CEngine::Instance()->GetCFG()->GetBool("r_enableProfiler") && CEngine::Instance()->GetCFG()->GetBool("r_enableRenderProfiler"))
	{
		m_pRenderSystem->GetRenderProfiler()->ProfileEnd(name);
	}
	else if (CEngine::Instance()->GetCFG()->GetBool("r_enableProfiler") && CEngine::Instance()->GetCFG()->GetBool("r_enableHardwareProfiler"))
	{
		// IssEmu::Todo - AMD GPUPerfapi | NVIDIA PerfKit
		throw std::logic_error("not implemented yet.");
	}

	return;
}
