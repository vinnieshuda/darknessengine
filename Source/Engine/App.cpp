#include "stdafx.h"
#include "App.h"

CApp* CApp::m_CApp = NULL;

CApp::CApp()
{
	m_hWnd = NULL;
	m_HInstance = NULL;
	m_screenWidth = m_screenHeight = 0;
	m_IsFullscreen = false;
	m_CApp = this;

	m_appName = "Engine";
}


CApp::~CApp()
{
}

#ifdef ENGINE_PLATFORM_WINDOWS
bool CApp::Init(InitParams& initParams)
{
	m_HInstance = initParams.mHinstance;
	m_appName = initParams.Title;

	m_screenWidth = CEngine::Instance()->GetCFG()->GetInt("r_screenWidth");
	m_screenHeight = CEngine::Instance()->GetCFG()->GetInt("r_screenHeight");
	m_IsFullscreen = CEngine::Instance()->GetCFG()->GetBool("r_fullscreen");

	if (!InitializeWindowsWindow(m_screenWidth, m_screenHeight))
		return false;

	return true;
}

bool CApp::InitializeWindowsWindow(int& screenWidth, int& screenHeight)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int positionX, positionY;

	// Windows class setup
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = NULL;
	wc.cbWndExtra = NULL;
	wc.hInstance = m_HInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = L"WindowClass1";
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register class.
	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, L"Error registering class",
			L"Error", MB_OK | MB_ICONERROR);
		return false;
	}

	if (m_IsFullscreen)
	{
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		positionX = positionY = 0;
	}
	else
	{
		// Place the window in the middle of the screen.
		positionX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
		positionY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

		m_hWnd = CreateWindowEx(NULL, L"WindowClass1", L"Window", WS_OVERLAPPEDWINDOW, positionX, positionY, screenWidth, screenHeight, NULL, NULL, m_HInstance, NULL);


	if (!m_hWnd)
	{
		MessageBox(NULL, L"Error creating window",
			L"Error", MB_OK | MB_ICONERROR);

		return false;
	}

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);	//Its good to update our window

	SetWindowTextA(m_hWnd, m_appName);

	return true;
}

void CApp::SetQuit(bool quitState)
{
	m_needQuit = quitState;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch (umessage)
	{
		// Check if the window is being destroyed.
		case WM_DESTROY:
			PostQuitMessage(0);
			exit(0);
			break;

		// Check if the window is being closed.
		case WM_CLOSE:
		{
			int retVal = MessageBox(NULL, L"Are you sure you want to quit?", L"Really?", MB_YESNO);
			if (retVal == IDYES)
			{
				CApp::GetCApp()->SetQuit(true);
			}
			return 0;
		}
		
		// Itt lesz a resize kezel�se
		case WM_SIZE:
			// LOWORD(lparam) = width, HIWORD(lparam) = height
			CEngine::Instance()->GetILog()->Print("Resized: (%i), (%i)", LOWORD(lparam), HIWORD(lparam));
			CEngine::Instance()->GetEventDispatcher()->OnEvent(eEvent_WindowResize, (unsigned int)wparam, (unsigned int)lparam);
		break;

		//////////////////////////////////////////////////////////////////////////
		// INPUT
		//////////////////////////////////////////////////////////////////////////
		// Check if a key has been pressed on the keyboard.
		case WM_KEYDOWN:
			// If a key is pressed send it to the input object so it can record that state.
			//m_Input->KeyDown((unsigned int)wparam);
			CEngine::Instance()->GetEventDispatcher()->OnEvent(eEvent_Input_KeyDown, (unsigned int)wparam, (unsigned int)lparam);
			break;

		// Check if a key has been released on the keyboard.
		case WM_KEYUP:
			// If a key is released then send it to the input object so it can unset the state for that key.
			//m_Input->KeyUp((unsigned int)wparam);
			CEngine::Instance()->GetEventDispatcher()->OnEvent(eEvent_Input_KeyUp, (unsigned int)wparam, (unsigned int)lparam);
			break;

		//////////////////////////////////////////////////////////////////////////
		// OTHER (e.g prevent ding sound)
		case WM_SYSKEYDOWN:
			if (wparam != VK_RETURN && wparam != VK_F4)
			{
				return 0;
			}
			else
			{
				if (wparam == VK_RETURN)
				{
					// Switch to fullscreen
				}
			}
		case WM_HOTKEY:
		case WM_SYSCHAR:
			return 0;
	}
	return DefWindowProc(hwnd, umessage, wparam, lparam);
}

bool CApp::Run()
{
	MSG msg;

	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));


	// Loop until there is a quit message from the window or the user.
	while (!m_needQuit)
	{
		// Handle the windows messages.
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if (msg.message == WM_QUIT)
		{
			m_needQuit = true;
		}
		else
		{
			// Otherwise do the frame processing.
			if (m_RenderSystem)
			{
				m_RenderSystem->Frame();
			}
		}

	}

	CEngine::Instance()->getAssetManager()->Release();
	CEngine::Instance()->getRenderSystem()->Release();

	return false;
}

void CApp::Release()
{

}
#endif // ENGINE_PLATFORM_WINDOWS

#ifdef ENGINE_PLATFORM_LINUX
#error "This platform is not implemented"
#endif // ENGINE_PLATFORM_LINUX

#ifdef ENGINE_PLATFORM_UNKNOWN
#error "UNKNOWN PLATFORM"
#endif // ENGINE_PLATFORM_UNKNOWN
