#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

class CApplication : IApplication 
{
	CApplication();
	virtual ~CApplication();

	// IApplication
	virtual bool Init(InitParams& initParams);
	virtual bool Run();
	virtual void Release();
	virtual HWND GetHWND();
	virtual void RegisterRenderSystem(IRenderSystem* pRenderSystem);
	virtual int GetWindowWidth();
	virtual int GetWindowHeight();
	virtual bool IsFullscreen();
	// ~IApplication

};

#endif // !__APPLICATION_H__
