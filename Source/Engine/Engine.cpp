#include "stdafx.h"
#include "Engine.h"
#include "Log.h"
#include "EngineConfig.h"
#include "App.h"
#include "EventDispatcher.h"

#include "RenderSystem.h"
#include "AssetManager.h"

CEngine* CEngine::m_pEngine = NULL;

CEngine::CEngine()
:m_pRenderSystem(NULL),
m_pLog(NULL),
m_pEngineCFG(NULL),
m_pApplication(NULL)
{
	m_pEngine = this;
}


CEngine::~CEngine()
{
}

CEngine* CEngine::Instance()
{
	if (!m_pEngine)
	{
		return NULL;
	}

	return m_pEngine;
}

bool CEngine::Init(InitParams& initParams)
{
	m_Timer.Reset();
	m_pLog = new CLog("Engine.log");
	
	m_pEngineCFG = new CEngineConfig(String(Environment::GetEngineDir()+"Bin/Engine.cfg").c_str());

	/************************************************************************/
	/* INITS                                                                */
	/************************************************************************/
	// EventDispatcher
	if (!InitializeEventDispatcher())
		return false;

	// Application
	m_pApplication = new CApp();
	if (!m_pApplication->Init(initParams))
		return false;

	/************************************************************************/
	/* SYSTEM INFO                                                          */
	/************************************************************************/
	CSystemInfo::Instance()->Init();
	// Log System Info
	m_pLog->Print("--------------------------*******SYSTEM INFO*******--------------------------");
	m_pLog->Print("CPU Model: %s", CSystemInfo::Instance()->GetCPUModel().c_str());
	m_pLog->Print("CPU Numcores: %d", CSystemInfo::Instance()->GetCPUNumCores());
	m_pLog->Print("CPU Intrinsics: %s", CSystemInfo::Instance()->GetCPUIntrinsicsList().c_str());
	m_pLog->Print("System Memory: %d MB", CSystemInfo::Instance()->GetSystemMemory());

	/************************************************************************/
	/* RENDER SYSTEM                                                        */
	/************************************************************************/
	switch (m_pEngineCFG->GetInt("r_renderSystem"))
	{
#ifdef ENGINE_PLATFORM_WINDOWS
		case eDriver_D3D9:
			//m_pRenderDevice = new CD3D9Render();
			break;
		case eDriver_D3D10:
			break;
		case eDriver_D3D11:
			m_pRenderSystem = new RenderSystem();
			break;
#endif
		case eDriver_OpenGL:
			break;
		case eDriver_NULL:
			break;
		default:
		{
			m_pRenderSystem = NULL;
			m_pLog->PrintError("UNKNOWN RENDER DRIVER");
			return false;
		}
	}

	m_pAssetManager = new AssetManager();
	m_pIAssetManager = m_pAssetManager;
	if (!m_pAssetManager->Init())
		return false;

	if (!m_pRenderSystem->Init(m_pApplication->GetWindowWidth(), m_pApplication->GetWindowHeight(), m_pApplication->GetHWND(), m_pApplication->IsFullscreen()))
		return false;

	m_pLog->Print("-----------------------------------------------------------------------------");
	m_pApplication->RegisterRenderSystem(m_pRenderSystem);

	m_pLog->Print("Version: %d.%d.%d", ENGINE_VERSION_MAJOR, ENGINE_VERSION_MINOR, ENGINE_VERSION_PATCH);
	m_pLog->Print("Engine Initialized!");

	m_pLog->Print("The Time between the engine initialization: %f seconds", m_Timer.getSeconds());

	return true;
}

bool CEngine::Run()
{
	if (!m_pApplication->Run())
	{		
		return false;
	}

	return true;
}

void CEngine::Release()
{
	m_pApplication->Release();
	m_pApplication = NULL;
}

bool CEngine::InitializeEventDispatcher()
{
	m_pEventDispatcher = new CEventDispatcher();
	if (!m_pEventDispatcher)
	{
		return false;
	}
	return true;
}

IRenderSystem* CEngine::getRenderSystem()
{
	return m_pRenderSystem;
}

ILog* CEngine::GetILog()
{
	return m_pLog;
}

IEngineConfig* CEngine::GetCFG()
{
	return m_pEngineCFG;
}

IApplication* CEngine::GetIApp()
{
	return m_pApplication;
}

IEventDispatcher* CEngine::GetEventDispatcher()
{
	return m_pEventDispatcher;
}
