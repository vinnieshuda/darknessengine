#include "stdafx.h"
#include "Log.h"
#include <chrono>
#include <ctime>
#include <iomanip> // put_time
#include <sstream>

static FILE* pFile = NULL;

const String getTime()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "[%Y.%m.%d-%H:%M:%S] ");
	return ss.str();
}

CLog::CLog()
{
}

CLog::~CLog()
{
}

CLog::CLog(const char* name)
{
	Init(name);
}

int CLog::Init(const char* name)
{
	assert(pFile == NULL);

	pFile = fopen(name, "wb");
	if (pFile == NULL) return 0;
	fflush(pFile);

	return 1;
}

void CLog::Print(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	char buffer[256];
	vsnprintf(buffer, 256, format, args);
	va_end(args);

	fprintf(pFile, getTime().c_str());

	printf("%s\n", buffer);

	fprintf(pFile, buffer);
	fprintf(pFile, "\n");
	fflush(pFile);
}

void CLog::PrintWarning(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	char buffer[256];
	vsnprintf(buffer, 256, format, args);
	va_end(args);

	fprintf(pFile, getTime().c_str());

	char tempbuff[256];
	sprintf_s(tempbuff, "[WARNING] - %s", buffer);
	strcpy(buffer, tempbuff);
	printf("%s\n", buffer);

	fprintf(pFile, buffer);
	fprintf(pFile, "\n");
	fflush(pFile);
}

void CLog::PrintError(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	char buffer[256];
	vsnprintf(buffer, 256, format, args);
	va_end(args);

	fprintf(pFile, getTime().c_str());

	char tempbuff[256];
	sprintf_s(tempbuff, "[ERROR] - %s", buffer);
	strcpy(buffer, tempbuff);
	printf("%s\n", buffer);

	fprintf(pFile, buffer);
	fprintf(pFile, "\n");
	fflush(pFile);
}