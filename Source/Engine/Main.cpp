#include "stdafx.h"
#include "Engine.h"

#if !defined(_LIB) && !defined(LINUX) && !defined(PS3)
BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:


		break;
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}
#endif

extern "C"
{
	ENGINE_API IEngine* CreateEngineInterface(InitParams& initParams)
	{
		CEngine *pEngine = new CEngine();

		if (!pEngine->Init(initParams))
		{
			delete pEngine;
			pEngine = NULL;

			return 0;
		}

		return pEngine;
	}
}