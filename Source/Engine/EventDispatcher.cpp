#include "stdafx.h"
#include "EventDispatcher.h"


CEventDispatcher::CEventDispatcher()
{
}

CEventDispatcher::~CEventDispatcher()
{
}

bool CEventDispatcher::AddListener(IEventListener* pListener)
{
	if (pListener && std::find(m_eventListeners.begin(), m_eventListeners.end(), pListener) == m_eventListeners.end())
	{
		m_eventListeners.push_back(pListener);
		return true;
	}

	return false;
}

bool CEventDispatcher::RemoveListener(IEventListener* pListener)
{
	EventListeners::iterator i = std::find(m_eventListeners.begin(), m_eventListeners.end(), pListener);
	if (i != m_eventListeners.end())
	{
		m_eventListeners.erase(i);
		return true;
	}

	return false;
}

void CEventDispatcher::OnEvent(EEventType eventType, UINT_PTR wparam, UINT_PTR lparam)
{
	for (EventListeners::iterator i = m_eventListeners.begin(); i != m_eventListeners.end(); ++i)
	{
		(*i)->OnEvent(eventType, wparam, lparam);
	}
}