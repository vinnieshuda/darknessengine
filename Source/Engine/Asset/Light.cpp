#include "stdafx.h"
#include "Light.h"


LightEntity::LightEntity()
{
	fColor3 = bgfx::createUniform("fColor3", bgfx::UniformType::Uniform3fv);
	fDirection3 = bgfx::createUniform("fDirection3", bgfx::UniformType::Uniform3fv);
	fAmbientIntensity1 = bgfx::createUniform("fAmbientIntensity1", bgfx::UniformType::Uniform1f);
}


LightEntity::~LightEntity()
{
}

void LightEntity::setColor(float R, float G, float B)
{
	color[0] = R;
	color[1] = G;
	color[2] = B;
}

void LightEntity::setIntensity(float in_intensity)
{
	intensity = in_intensity;
}

void LightEntity::setDirection(float x, float y, float z)
{
	direction[0] = x;
	direction[1] = y;
	direction[2] = z;
	setPosition(x, y, z);
}

void LightEntity::setUniforms()
{
	bgfx::setUniform(fColor3, &color);
	bgfx::setUniform(fDirection3, &direction);
	bgfx::setUniform(fAmbientIntensity1, &intensity);
}
