#ifndef _ENTITY_H__
#define _ENTITY_H__
#pragma once

#include "IEntity.h"
#include "Mesh.h"

class Entity : public IEntity
{
public:

	Entity();
	virtual ~Entity();

	virtual bool Update(float time);
	virtual void Draw();
	virtual void Destroy();

	int randomseed;

	pos getPosition(){ return position; }
	rot getRotation(){ return rotation; }

	void setPosition(float x = 0, float y = 0, float z = 0){ position.x = x; position.y = y; position.z = z; updateMeshPos(position, rotation); }
	void setPosition(pos position3d){ position = position3d; updateMeshPos(position, rotation); }
	void setRotation(float angleX = 0, float angleY = 0, float angleZ = 0){ rotation.x = angleX, rotation.y = angleY, rotation.z = angleZ; updateMeshPos(position, rotation); }
	void setRotation(rot rotation3d) { rotation = rotation3d; updateMeshPos(position, rotation); }
	
	void CreateMesh(const char* _filePath);
	void updateMeshPos(pos position, rot rotation){ if (!&mesh) return; mesh.setPos(position.x, position.y, position.z); mesh.rotateXYZ(rotation.x, rotation.y, rotation.z); }
	Mesh* getMesh(){ return &mesh; }

	int getID(){ return EntityID; }
	void setID(int entityID){ EntityID = entityID; }

	private:
	pos position;
	rot rotation;
	Mesh mesh;
	int EntityID;

};

#endif

