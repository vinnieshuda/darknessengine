#include "stdafx.h"
#include "Entity.h"


Entity::Entity()
{
	EntityID = 1;

	randomseed = rand() % 3;
}


Entity::~Entity()
{
}

bool Entity::Update(float time)
{
	
	updateMeshPos(position, rotation);
	return true;
}

void Entity::Destroy()
{
	getMesh()->UnLoad();
}

void Entity::Draw()
{

}

void Entity::CreateMesh(const char* _filePath)
{
	mesh.Load(_filePath);
}