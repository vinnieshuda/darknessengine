#pragma once

#include "IMesh.h"
#include <bgfx.h>
#include <bx/readerwriter.h>
#include <bx/fpumath.h>

#include <string.h> // strlen
#include <bgfx.h>
#include <tinystl/allocator.h>
#include <tinystl/vector.h>
#include <tinystl/string.h>
namespace stl = tinystl;

struct Aabb
{
	float m_min[3];
	float m_max[3];
};

struct Obb
{
	float m_mtx[16];
};

struct Sphere
{
	float m_center[3];
	float m_radius;
};

struct Primitive
{
	uint32_t m_startIndex;
	uint32_t m_numIndices;
	uint32_t m_startVertex;
	uint32_t m_numVertices;

	Sphere m_sphere;
	Aabb m_aabb;
	Obb m_obb;
};

typedef stl::vector<Primitive> PrimitiveArray;

struct Group
{
	Group()
	{
		reset();
	}

	void reset()
	{
		m_vbh.idx = bgfx::invalidHandle;
		m_ibh.idx = bgfx::invalidHandle;
		m_prims.clear();
	}

	bgfx::VertexBufferHandle m_vbh;
	bgfx::IndexBufferHandle m_ibh;
	Sphere m_sphere;
	Aabb m_aabb;
	Obb m_obb;
	PrimitiveArray m_prims;
};

struct pos
{
	float x;
	float y;
	float z;

	bool operator==(const pos& rhs) const
	{
		return (x == rhs.x)
			|| (y == rhs.y)
			|| (z == rhs.z);
	}

	bool operator!=(const pos& rhs) const
	{
		return !operator==(rhs);
	}

	/*bool operator!=(const pos &in) const {
		const pos &self = (*this);
		return self.x != in.x || self.y != in.y || self.z != in.z;
	}

	bool operator==(const pos &in) const {
		const pos &self = (*this);
		return self.x == in.x && self.y == in.y && self.z == in.z;
	}*/
};

struct rot
{
	float x;
	float y;
	float z;

	bool operator==(const rot& rhs) const
	{
		return (x == rhs.x)
			|| (y == rhs.y)
			|| (z == rhs.z);
	}

	bool operator!=(const rot& rhs) const
	{
		return !operator==(rhs);
	}

	/*bool operator!=(const rot &in) const {
		const rot &self = (*this);
		return self.x != in.x || self.y != in.y || self.z != in.z;
	}

	bool operator==(const rot &in) const {
		const rot &self = (*this);
		return self.x == in.x && self.y == in.y && self.z == in.z;
	}*/
};

namespace bgfx
{
	int32_t read(bx::ReaderI* _reader, bgfx::VertexDecl& _decl);
}

class Mesh : public IMesh
{
public:
	Mesh();
	~Mesh();

	///////////////////////////////////////////////////////
	// Virtual functions
	//////////////////////////////////////////////////////

	virtual void Load(const char* _filePath);
	virtual void UnLoad();
	virtual void Draw(uint8_t _id, bgfx::ProgramHandle _program, /*float* _mtx,*/uint64_t _state = BGFX_STATE_MASK);

	///////////////////////////////////////////////////////
	// Positional functions
	//////////////////////////////////////////////////////

	float* getMTX() { return mtx; }
	void clearMTX()
	{
		for (int i = 0;i<16;i++)
		{
			mtx[i] = 0;
		}
	}

	void setPos(float x, float y, float z){ bx::mtxTranslate(mtx, x, y, z); }
	pos getPos() { pos ret; ret.x = mtx[12]; ret.y = mtx[13]; ret.z = mtx[14];return ret;}
	void rotateX(float angle){ bx::mtxRotateX(mtx, angle); }
	void rotateY(float angle){ bx::mtxRotateY(mtx, angle); }
	void rotateZ(float angle){ bx::mtxRotateZ(mtx, angle); }
	void rotateXY(float anglex, float angley) { bx::mtxRotateXY(mtx, anglex, angley); }
	void rotateXYZ(float anglex, float angley, float anglez) { bx::mtxRotateXYZ(mtx, anglex, angley, anglez); }
	void scale(float x, float y, float z) { bx::mtxScale(mtx, x, y, z);}

	typedef stl::vector<Group> GroupArray;
	GroupArray m_groups;

	public:
	float mtx[16];
};

