#ifndef _ASSETMANAGER_H__
#define _ASSETMANAGER_H__

#pragma once

#include "IAssetManager.h"
#include "Entity.h"

#include <bx/readerwriter.h>
#include <bx/fpumath.h>

#include <string>
#include <vector>
#include <algorithm>

class AssetManager : public IAssetManager
{
	public:
		AssetManager();
		~AssetManager();
    
		virtual bool Init();
		virtual void Release();

		//Entity functions
		std::vector<Entity>* getEntityList(){return &EntityList;}

		virtual bool AddEntity(const char* _filepath);
		Entity createEntity(const char* _filepath);
		virtual void RemoveEntity(Entity* entity);

		//Shader
		bgfx::ProgramHandle loadShaders(const char* VSfilePath, const char* FSfilepath/*, const char* shaderName*/);

		int meshNum;

	private:
		std::vector<Entity> EntityList;

};

#endif