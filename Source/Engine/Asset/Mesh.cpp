#include "stdafx.h"
#include "Mesh.h"
#include "IAssetManager.h"
Mesh::Mesh()
{
	memset(mtx, 0, sizeof(float)* 16);
}

Mesh::~Mesh()
{

}

void Mesh::Load(const char* _filePath)
{
	//pathName = _strdup(_filePath);
	bgfx::VertexDecl m_decl;

#define BGFX_CHUNK_MAGIC_VB  BX_MAKEFOURCC('V', 'B', ' ', 0x1)
#define BGFX_CHUNK_MAGIC_IB  BX_MAKEFOURCC('I', 'B', ' ', 0x0)
#define BGFX_CHUNK_MAGIC_PRI BX_MAKEFOURCC('P', 'R', 'I', 0x0)

	bx::CrtFileReader reader;
	reader.open(_filePath);

	Group group;

	uint32_t chunk;
	while (4 == bx::read(&reader, chunk))
	{
		switch (chunk)
		{
		case BGFX_CHUNK_MAGIC_VB:
		{
									bx::read(&reader, group.m_sphere);
									bx::read(&reader, group.m_aabb);
									bx::read(&reader, group.m_obb);

									bgfx::read(&reader, m_decl);
									uint16_t stride = m_decl.getStride();

									uint16_t numVertices;
									bx::read(&reader, numVertices);
									const bgfx::Memory* mem = bgfx::alloc(numVertices*stride);
									bx::read(&reader, mem->data, mem->size);

									group.m_vbh = bgfx::createVertexBuffer(mem, m_decl);
		}
			break;

		case BGFX_CHUNK_MAGIC_IB:
		{
									uint32_t numIndices;
									bx::read(&reader, numIndices);
									const bgfx::Memory* mem = bgfx::alloc(numIndices * 2);
									bx::read(&reader, mem->data, mem->size);
									group.m_ibh = bgfx::createIndexBuffer(mem);
		}
			break;

		case BGFX_CHUNK_MAGIC_PRI:
		{
									 uint16_t len;
									 bx::read(&reader, len);

									 std::string material;
									 material.resize(len);
									 bx::read(&reader, const_cast<char*>(material.c_str()), len);

									 uint16_t num;
									 bx::read(&reader, num);

									 for (uint32_t ii = 0; ii < num; ++ii)
									 {
										 bx::read(&reader, len);

										 std::string name;
										 name.resize(len);
										 bx::read(&reader, const_cast<char*>(name.c_str()), len);

										 Primitive prim;
										 bx::read(&reader, prim.m_startIndex);
										 bx::read(&reader, prim.m_numIndices);
										 bx::read(&reader, prim.m_startVertex);
										 bx::read(&reader, prim.m_numVertices);
										 bx::read(&reader, prim.m_sphere);
										 bx::read(&reader, prim.m_aabb);
										 bx::read(&reader, prim.m_obb);

										 group.m_prims.push_back(prim);
									 }

									 m_groups.push_back(group);
									 group.reset();
		}
			break;

		default:
			break;
		}
	}

	reader.close();
}

void Mesh::UnLoad()
{
	for (auto it = m_groups.begin(), itEnd = m_groups.end(); it != itEnd; ++it)
	{
		const Group& group = *it;
		bgfx::destroyVertexBuffer(group.m_vbh);

		if (bgfx::isValid(group.m_ibh))
		{
			bgfx::destroyIndexBuffer(group.m_ibh);
		}
	}
	m_groups.clear();
	clearMTX();
}
void  Mesh::Draw(uint8_t _id, bgfx::ProgramHandle _program, /*float* _mtx,*/ uint64_t _state)
{
	if (BGFX_STATE_MASK == _state)
	{
		_state = 0
			| BGFX_STATE_RGB_WRITE
			| BGFX_STATE_ALPHA_WRITE
			| BGFX_STATE_DEPTH_WRITE
			| BGFX_STATE_DEPTH_TEST_LESS
			| BGFX_STATE_CULL_CCW
			| BGFX_STATE_MSAA
			;
	}

	for (GroupArray::const_iterator it = m_groups.begin(), itEnd = m_groups.end(); it != itEnd; ++it)
	{
		const Group& group = *it;

		// Set model matrix for rendering.
		bgfx::setTransform(mtx);
		bgfx::setProgram(_program);
		bgfx::setIndexBuffer(group.m_ibh);
		bgfx::setVertexBuffer(group.m_vbh);
		bgfx::setState(_state);
		bgfx::submit(_id);
	}
}

