#include "stdafx.h"
#include "AssetManager.h"

AssetManager::AssetManager()
{

}

AssetManager::~AssetManager()
{

}

long int fsize(FILE* _file)
{
	long int pos = ftell(_file);
	fseek(_file, 0L, SEEK_END);
	long int size = ftell(_file);
	fseek(_file, pos, SEEK_SET);
	return size;
}

bool AssetManager::Init()
{
	CEngine::Instance()->GetILog()->Print("Asset Manager Initialized!");
	EntityList.clear();

	return true;
}

void AssetManager::Release()
{
	for (auto it = getEntityList()->begin(); it != getEntityList()->end(); ++it)
	{
		it->Destroy();
	}
	EntityList.clear();
}

bool AssetManager::AddEntity(const char* _filepath)
{
	Entity newEntity;

	float rand1 = rand() % 10;
	float rand2 = rand() % 10;
	float rand3 = rand() % 20;
	float rotrand1 = rand() % 50;
	float rotrand2 = rand() % 50;
	float rotrand3 = rand() % 50;
	float randasd = rand() % 3;
	float randasd2 = rand() % 3;

	if (randasd >= 2)
		rand1 = -rand1;
	if (randasd2 >= 2)
		rand2 = -rand2;

	newEntity.setPosition(rand1, rand2, rand3);
	newEntity.setRotation(0.0f, 90.0f, 0.0f);
	newEntity.CreateMesh(_filepath);

	EntityList.push_back(newEntity);

	return true;
}

Entity AssetManager::createEntity(const char* _filepath)
{
	Entity newEntity;

	newEntity.CreateMesh(_filepath);

	newEntity.setPosition(1, 1, 1);
	newEntity.setRotation(1, 1, 1);

	return newEntity;

}

void AssetManager::RemoveEntity(Entity* entity)
{
	for (auto it = EntityList.begin(); it != EntityList.end(); ++it)
	{
/*		EntityList.erase(it);*/
	}
}

// Shader Functions

static const bgfx::Memory* load(const char* _filePath)
{
	FILE* file = fopen(_filePath, "rb");
	if (NULL != file)
	{
		uint32_t size = (uint32_t)fsize(file);
		const bgfx::Memory* mem = bgfx::alloc(size + 1);
		size_t ignore = fread(mem->data, 1, size, file);
		BX_UNUSED(ignore);
		fclose(file);
		mem->data[mem->size - 1] = '\0';
		return mem;
	}

	return NULL;
}

static const bgfx::Memory* loadShader(const char* _name)
{
	return load(_name);
}

bgfx::ProgramHandle AssetManager::loadShaders(const char* VSfilePath, const char* FSfilepath/*, const char* shaderName*/)
{
	const bgfx::Memory* mem;

	// Load vertex shader.
	mem = loadShader(VSfilePath);
	bgfx::ShaderHandle vsh = bgfx::createShader(mem);

	// Load fragment shader.
	mem = loadShader(FSfilepath);
	bgfx::ShaderHandle fsh = bgfx::createShader(mem);

	// Create program from shaders.
	return bgfx::createProgram(vsh, fsh, true /* destroy shaders when program is destroyed */);
}