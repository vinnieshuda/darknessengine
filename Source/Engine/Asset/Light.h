#pragma once

class LightEntity : public Entity
{
	public:
		LightEntity();
		~LightEntity();

		bgfx::UniformHandle fColor3;
		bgfx::UniformHandle fDirection3;
		bgfx::UniformHandle fAmbientIntensity1;

		float color[3];
		float direction[3];
		float intensity;

		void setColor(float R, float G, float B);
		void setIntensity(float intensity);
		void setDirection(float x, float y, float z);

		void setUniforms();

};

