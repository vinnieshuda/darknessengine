#ifndef __CGPUPROFILER_H__
#define __CGPUPROFILER_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#include <IGPUProfiler.h>

class CGPUProfiler : public IGPUProfiler
{
public:
	CGPUProfiler(IRenderSystem* pRenderSystem);
	~CGPUProfiler();

	// IGPUProfiler
	virtual GPUInfos* GetGPUInfo() { return m_pRenderSystem->GetGPUInfos(); }

	virtual void ProfileStart(String name);
	virtual void ProfileEnd(String name);
	// ~IGPUProfiler

private:
	IRenderSystem* m_pRenderSystem;
};

#endif // !__CGPUPROFILER_H__