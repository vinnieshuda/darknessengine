#ifndef __CAPP_H__
#define __CAPP_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)


class CApp : public IApplication
{
public:
	CApp();
	virtual ~CApp();

	// IApplication
	virtual bool Init(InitParams& initParams);
	virtual bool Run();
	virtual void Release();
	virtual HWND GetHWND() { return m_hWnd; }
	virtual void RegisterRenderSystem(IRenderSystem* pRenderSystem) { m_RenderSystem = pRenderSystem; }

	virtual int GetWindowWidth() { return m_screenWidth; }
	virtual int GetWindowHeight() { return m_screenHeight; }
	virtual bool IsFullscreen() { return m_IsFullscreen; }
	// ~IApplication

	static CApp* GetCApp() { return m_CApp; }
	void SetQuit(bool quitState);

#ifdef ENGINE_PLATFORM_WINDOWS
	bool InitializeWindowsWindow(int& screenWidth, int& screenHeight);
	
#endif // ENGINE_PLATFORM_WINDOWS

private:
	IRenderSystem* m_RenderSystem;

private:
	HWND m_hWnd;
	HINSTANCE m_HInstance;
	int m_screenWidth, m_screenHeight;
	bool m_IsFullscreen;

	const char* m_appName;
	static CApp* m_CApp;

	bool m_needQuit;
};

#ifdef ENGINE_PLATFORM_WINDOWS
	static LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam);
#endif /

#endif // !__CAPP_H__