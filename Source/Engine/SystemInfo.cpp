#include "stdafx.h"
#include "SystemInfo.h"

#if defined(ENGINE_PLATFORM_WINDOWS)
#include <intrin.h>

static inline void cpuid(int* ret, int feature)
{
	__cpuid(ret, feature);
}
#elif defined(ENGINE_PLATFORM_LINUX)
#include <unistd.h>
#include <sys/sysctl.h>
#include <X11/Xlib.h>
#include <cpuid.h>

static inline void cpuid(int* ret, int feature)
{
	__get_cpuid(feature, &ret[0], &ret[1], &ret[2], &ret[3]);
}
#endif

// cpuid BitFlags
#define SSE_SUPPORTED			0x02000000
#define SSE2_SUPPORTED			0x04000000

CSystemInfo* CSystemInfo::m_pSystemInfo = NULL;

CSystemInfo* CSystemInfo::Instance()
{
	if (!m_pSystemInfo)
	{
		m_pSystemInfo = new CSystemInfo;
	}

	return m_pSystemInfo;
}

CSystemInfo::CSystemInfo()
: numCPUCores(0), systemMemory(0)
{
	memset(&CPUInfo, 0, sizeof(CPUInfo));
}

CSystemInfo::~CSystemInfo()
{
}

bool CSystemInfo::Init()
{
#ifdef ENGINE_PLATFORM_DESKTOP

	// CPUID SUPPORTED?
	__try
	{
		_asm
		{
			xor eax, eax
				cpuid
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		return false;
	}

	QueryCPU();

	// Inits
	InitCPUInfo();
	InitNumCores();
	InitSystemMemory();
#endif // ENGINE_PLATFORM_DESKTOP
	return true;
}

void CSystemInfo::QueryCPU()
{
#ifdef ENGINE_PLATFORM_DESKTOP
	union {
		char Buffer[12 + 1];
		struct {
			unsigned int ebx;
			unsigned int edx;
			unsigned int ecx;
		} stc;
	} Vendor;
	memset(&Vendor, 0, sizeof(Vendor));

	struct CPUQuery
	{
		unsigned int eax;
		unsigned int ebx;
		unsigned int ecx;
		unsigned int edx;
	};

	CPUQuery query;

	cpuid((int*)&query, 0x00);
	unsigned int eaxRet = query.eax;

	Vendor.stc.ebx = query.ebx;
	Vendor.stc.edx = query.edx;
	Vendor.stc.ecx = query.ecx;

	strcpy(CPUInfo.VendorName, Vendor.Buffer);
	
	// Intrinsics
	if (eaxRet >= 1)
	{
		cpuid((int*)&query, 0x01);
		if (query.edx & (1 << 25))
			m_CPUIntrinsicsList.append("SSE|");
		if (query.edx & (1 << 26))
			m_CPUIntrinsicsList.append("SSE2|");
		if (query.ecx & (1 << 0)) 
			m_CPUIntrinsicsList.append("SSE3|");
		if (query.ecx & (1 << 9)) 
			m_CPUIntrinsicsList.append("SSSE3|");
		if (query.ecx & (1 << 19)) 
			m_CPUIntrinsicsList.append("SSE41|");
		if (query.ecx & (1 << 20)) 
			m_CPUIntrinsicsList.append("SSE42|");
		if (query.edx & (1 << 28)) 
			m_CPUIntrinsicsList.append("HTT|");
	}
	
	// CPU Model
	cpuid((int*)&query, 0x80000000);
	if (query.eax > 0)
	{
		cpuid((int*)&CPUInfo.ProcessorName, 0x80000002);
		cpuid((int*)(CPUInfo.ProcessorName + 16), 0x80000003);
		cpuid((int*)(CPUInfo.ProcessorName + 32), 0x80000004);
	}
#endif // ENGINE_PLATFORM_DESKTOP
}

bool CSystemInfo::InitNumCores()
{
#ifdef ENGINE_PLATFORM_WINDOWS
	SYSTEM_INFO system;
	GetSystemInfo(&system);
	numCPUCores = system.dwNumberOfProcessors;
#endif
#ifdef ENGINE_PLATFORM_LINUX
	FILE* pfile = fopen("/proc/cpuinfo", "r");
	if (pfile) 
	{
		char buf[1024];
		while (fgets(buf, sizeof(buf), pfile)) {
			char *t = strchr(buf, ':');
			if (t++ == NULL) continue;
			while (*t == ' ') t++;
			if (!strncmp(buf, "processor", 9)) {
				sscanf(t "%d", &numCPUCores);
				numCPUCores++;
			}
		}
		fclose(pfile);
	}
	else
	{
		CEngine::Instance()->GetILog()->PrintError("Failed to get CPU core number [CSystemInfo::InitNumCores]");
	}
#endif

	return true;
}

void CSystemInfo::InitCPUInfo()
{
	String vendor(CPUInfo.VendorName);

	// vendors
	// TODO: ARM DETECT
	if (vendor.compare("GenuineIntel") == 0)
	{
		m_CPUVendor = eCPU_Intel;
		m_CPUVendorString = "Intel";
	}
	else if (vendor.compare("AuthenticAMD") == 0)
	{
		m_CPUVendor = eCPU_AMD;
		m_CPUVendorString = "AMD";
	}
	else
	{
		m_CPUVendor = eCPU_UNKNOWN;
		m_CPUVendorString = "UNKNOWN";
	}
}

void CSystemInfo::InitSystemMemory()
{
#if defined(ENGINE_PLATFORM_WINDOWS)
	MEMORYSTATUS memory;
	memset(&memory, 0, sizeof(memory));
	memory.dwLength = sizeof(memory);
	GlobalMemoryStatus(&memory);
	systemMemory = (int)(memory.dwTotalPhys / 1024 / 1024);
#else
	// TODO: Linux, mac, winrt, ...
#error Implement
#endif // defined(ENGINE_PLATFORM_WINDOWS)
}
