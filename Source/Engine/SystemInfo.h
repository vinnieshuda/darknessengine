#ifndef __CSYSTEMINFO_H__
#define __CSYSTEMINFO_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#include <vector>

class CSystemInfo
{
public:
	static CSystemInfo* Instance();

	bool Init();

	ECPUVendor GetCPUVendor() { return m_CPUVendor; }
	String GetCPUVendorString() { return m_CPUVendorString; }
	String GetCPUModel() { return CPUInfo.ProcessorName; }
	String GetCPUIntrinsicsList() { return m_CPUIntrinsicsList; }
	int GetCPUNumCores() { return numCPUCores; }

	int GetSystemMemory() { return systemMemory; }

private:
	CSystemInfo();
	~CSystemInfo();

	static CSystemInfo* m_pSystemInfo;

	bool InitNumCores();
	void QueryCPU();
	void InitCPUInfo();
	void InitSystemMemory();

	ECPUVendor m_CPUVendor;
	String m_CPUVendorString;
	int numCPUCores;

	int systemMemory;

	String m_CPUIntrinsicsList;

	struct _CPU_INFO
	{
		char VendorName[14];
		char ProcessorName[48];
	};
	struct _CPU_INFO CPUInfo;
};

#endif // !__CSYSTEMINFO_H__