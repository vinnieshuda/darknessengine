#ifndef __CGAMEFRAMEWORK_H__
#define __CGAMEFRAMEWORK_H__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

class CGameFramework : public IGameFramework, public IEventListener
{
public:
	CGameFramework();
	~CGameFramework();

	// IGameFramework
	virtual bool Init();
	virtual bool Run();
	virtual void Release();
	// ~IGameFramework

	// IEventListener
	virtual void OnEvent(EEventType eventType, UINT_PTR wparam, UINT_PTR lparam);
	// ~IEventListener

	int test;
};

#endif // __CGAMEFRAMEWORK_H__