#include "stdafx.h"
#include <iostream>
#include <string>
#include <IEngine.h>
#include <ILog.h>
#include "GameFramework.h"

HMODULE			m_hEngineDll = NULL;
IEngineConfig* pConfig;
IEngine* pEngine = NULL;
CGameFramework* m_gameFramework = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	m_hEngineDll = LoadLibrary("Engine.dll");

	if (!m_hEngineDll)
	{
		MessageBox(0, "Failed to load the Engine DLL!", "Error", MB_ICONERROR | MB_OK | MB_DEFAULT_DESKTOP_ONLY);
		// failed to load the dll

		return false;
	}

	PFNCREATEENGINEINTERFACE pfnCreateEngineInterface = (PFNCREATEENGINEINTERFACE)::API_FUNC(m_hEngineDll, "CreateEngineInterface");

	InitParams initParams;
	initParams.mHinstance = GetModuleHandle(NULL);
	initParams.Title = "CEngine Test";


	pEngine = pfnCreateEngineInterface(initParams);

	if (!pEngine)
	{

		MessageBox(0, "CreateEngineInterface Failed!", "Error", MB_ICONERROR | MB_OK | MB_DEFAULT_DESKTOP_ONLY);
		return false;
	}

/*
	// TODO: REMOVE
	pEngine->GetILog()->Print("RenderDriver: %i", pEngine->getRenderSystem()->getRenderDriverType());
	pEngine->GetILog()->Print("RenderDriverString: %s", pEngine->getRenderSystem()->getRenderDriverTypeString().c_str());
	pEngine->GetILog()->Print("CurrDir: %s", Environment::GetCurrentDir().c_str());*/
	pEngine->GetILog()->Print("EngineDir: %s", Environment::GetEngineDir().c_str());
	pConfig = pEngine->GetCFG();

	m_gameFramework = new CGameFramework();
	m_gameFramework->Init();

	if (!pEngine->Run())
	{
		m_gameFramework->Release();
		pEngine->Release();
	}

	return 0;
}