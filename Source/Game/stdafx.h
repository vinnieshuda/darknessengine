#ifndef __stdafx_h__
#define __stdafx_h__

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif // defined(_MSC_VER)

#include <assert.h>
#include <stdio.h>
#include <stdarg.h>
#include <platform.h>

#if !defined(PS3)
#include <memory.h>
#include <malloc.h>
#endif

#if !defined(PS3)
#if defined( LINUX )
#	include <sys/io.h>
#else
#	include <io.h>
#endif
#endif

#ifdef ENGINE_PLATFORM_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <tlhelp32.h>
#undef GetCharWidth
#undef GetUserName
#endif

//////////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////////
#include <IEngine.h>
#include <IRenderSystem.h>
#include <ILog.h>
#include <IApplication.h>
#include <IGameFramework.h>
#include <IEventListener.h>

//////////////////////////////////////////////////////////////////////////
// Mathlib
//////////////////////////////////////////////////////////////////////////
#include <MathLib.h>

//////////////////////////////////////////////////////////////////////////
// Engine Global
//////////////////////////////////////////////////////////////////////////
extern IEngine* pEngine;

#endif